<?php
    require 'template.php';
    session_start();
    $carrito = 0;
    if(isset($_SESSION["carrito"])){
        $carrito = $_SESSION["carrito"];
    }
    $logged = false;
    if(isset($_SESSION["id_usuario"])){
        $id_usuario = $_SESSION["id_usuario"];
        $logged = true;
    }
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>

</html>