package Formularios;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.DPFPDataAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPErrorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPErrorEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.capture.event.DPFPSensorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPSensorEvent;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import BD.MySqlConn;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.JLabel;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;


/**
 *
 * @author Fuantox
 * 
 * 
 */


public class CapturaHuella extends javax.swing.JDialog {
    
    int consumos = 0;
    String barcode = "";
    String auxCode = "";
    
    //Variable que permite iniciar el dispositivo de lector de huella conectado
    // con sus distintos metodos.
    private DPFPCapture Lector = DPFPGlobal.getCaptureFactory().createCapture();

    //Variable que permite establecer las capturas de la huellas, para determina sus caracteristicas
    // y poder estimar la creacion de un template de la huella para luego poder guardarla
    private DPFPEnrollment Reclutador = DPFPGlobal.getEnrollmentFactory().createEnrollment();

    //Esta variable tambien captura una huella del lector y crea sus caracteristcas para auntetificarla
    // o verificarla con alguna guardada en la BD
    private DPFPVerification Verificador = DPFPGlobal.getVerificationFactory().createVerification();

    //Variable que para crear el template de la huella luego de que se hallan creado las caracteriticas
    // necesarias de la huella si no ha ocurrido ningun problema
    private DPFPTemplate template;
    public static String TEMPLATE_PROPERTY = "template";

    protected void Iniciar(){
        jTextPane1.setContentType("text/html");
        
        //DataListener: Huella capturada.
        Lector.addDataListener(new DPFPDataAdapter() {
            @Override 
            public void dataAcquired(final DPFPDataEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        EnviarTexto("La Huella Digital ha sido Capturada");
                        ProcesarCaptura(e.getSample());
                    }
                }
            );
        }
        });
        
        
        
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    // your code is scanned and you can access it using frame.getBarCode()
                    // now clean the bar code so the next one can be read
                    barcode = auxCode;
                    auxCode = "";
                    identificarCodigo(barcode);
                } else {
                    // some character has been read, append it to your "barcode cache"
                    if(e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_Z){
                        auxCode += e.getKeyChar();
                        System.out.println(e.getKeyChar());
                    }
                }
            }

        });

        //DataListener: Lector Conectado/Desconectado
        Lector.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            @Override                        
            public void readerConnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {	
                    public void run() {
                        EnviarTexto("El Sensor de Huella Digital esta Activado o Conectado");
                    }
                });
            }
            
            @Override 
            public void readerDisconnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {	
                    public void run() {
                        EnviarError("El Sensor de Huella Digital está Desactivado o no Conectado");
                    }
                });
            }
        });

        //DataListener: Dedo sobre lector/Dedo fuera lector.
        Lector.addSensorListener(new DPFPSensorAdapter() {
            @Override 
            public void fingerTouched(final DPFPSensorEvent e) {
                SwingUtilities.invokeLater(new Runnable() {	
                    public void run() {
                        EnviarTexto("El dedo ha sido colocado sobre el Lector de Huella");
                    }
                });
            }
    
            @Override 
            public void fingerGone(final DPFPSensorEvent e) {
                SwingUtilities.invokeLater(new Runnable() {	
                    public void run() {
                        EnviarTexto("El dedo ha sido quitado del Lector de Huella");
                    }
                });
            }
        });

        //DataListener: Error de lectura
        Lector.addErrorListener(new DPFPErrorAdapter(){
            public void errorReader(final DPFPErrorEvent e){
                SwingUtilities.invokeLater(new Runnable() {  
                    public void run() {
                        EnviarError("Error: "+e.getError());
                    }
                });
            }
        }); 
        
        
    }
    
    public DPFPFeatureSet featuresinscripcion;
    public DPFPFeatureSet featuresverificacion;
    
    public  DPFPFeatureSet extraerCaracteristicas(DPFPSample sample, DPFPDataPurpose purpose){
        DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        try {
            return extractor.createFeatureSet(sample, purpose);
        } 
        catch (DPFPImageQualityException e) {
            return null;
        }
    }

    public  Image CrearImagenHuella(DPFPSample sample) {
        return DPFPGlobal.getSampleConversionFactory().createImage(sample);
    }

    public void DibujarHuella(Image image) {
        lblImagenHuella.setIcon(new ImageIcon(
            image.getScaledInstance(lblImagenHuella.getWidth(), lblImagenHuella.getHeight(), Image.SCALE_DEFAULT)
        ));
        repaint();
    }
    
    
    public  void ProcesarCaptura(DPFPSample sample)
    {
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de inscripción.
        featuresinscripcion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);

        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de verificacion.
        featuresverificacion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

        // Comprobar la calidad de la muestra de la huella y lo añade a su reclutador si es bueno
        if (featuresinscripcion != null)
            try{
                System.out.println("Las Caracteristicas de la Huella han sido creada");
                Reclutador.addFeatures(featuresinscripcion);// Agregar las caracteristicas de la huella a la plantilla a crear

                // Dibuja la huella dactilar capturada.
                Image image=CrearImagenHuella(sample);
                DibujarHuella(image);
                try {
                    identificarHuella();
                    Reclutador.clear();
                } catch (IOException ex) {
                    Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                }
     
                /*btnVerificar.setEnabled(true);
                btnIdentificar.setEnabled(true);*/

            }
            catch (DPFPImageQualityException ex) {
                System.err.println("Error: "+ex.getMessage());
            }
            finally {
                EstadoHuellas();
                // Comprueba si la plantilla se ha creado.
                switch(Reclutador.getTemplateStatus())
                {
                    case TEMPLATE_STATUS_READY:	// informe de éxito y detiene  la captura de huellas
                        stop();
                        setTemplate(Reclutador.getTemplate());
                        EnviarTexto("La Plantilla de la Huella ha Sido Creada, ya puede Verificarla o Identificarla");
                        btnIdentificar.setEnabled(false);
                        btnVerificar.setEnabled(false);
                        btnGuardar.setEnabled(true);
                        btnGuardar.grabFocus();
                    break;

                    case TEMPLATE_STATUS_FAILED: // informe de fallas y reiniciar la captura de huellas
                        Reclutador.clear();
                        stop();
                        EstadoHuellas();
                        setTemplate(null);
                        JOptionPane.showMessageDialog(CapturaHuella.this, "La Plantilla de la Huella no pudo ser creada, Repita el Proceso", "Inscripcion de Huellas Dactilares", JOptionPane.ERROR_MESSAGE);
                        start();
                    break;
                }
            }
    }
    
    public  void EstadoHuellas(){
	EnviarTexto("Muestra de Huellas Necesarias para Guardar Template "+ Reclutador.getFeaturesNeeded());
    }
    
    public void EnviarTexto(String string) {
        HTMLDocument doc = (HTMLDocument) jTextPane1.getStyledDocument();
        try {
            doc.insertAfterEnd(doc.getCharacterElement(doc.getLength()),"<span style=\"font-family: 'Montserrat';font-size: 9px;\">"+string+"</span><br>");
       } catch (Exception ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void EnviarError(String string) {
        HTMLDocument doc = (HTMLDocument) jTextPane1.getStyledDocument();
        try {
            doc.insertAfterEnd(doc.getCharacterElement(doc.getLength()),"<span style=\"font-family: 'Montserrat';font-size: 9px;color: #bf0000;\">"+string+"</span><br>");
       } catch (Exception ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  void start(){
	Lector.startCapture();
	EnviarTexto("Utilizando el Lector de Huella Dactilar ");
    }

    /** Creates new form CapturaHuella */
    public CapturaHuella() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Imposible modificar el tema visual", "Lookandfeel inválido.",
            JOptionPane.ERROR_MESSAGE);
        }
        initComponents();
    }
    
    public  void stop(){
        Lector.stopCapture();
        EnviarError("No se está usando el Lector de Huella Dactilar ");
    }

    public DPFPTemplate getTemplate() {
        return template;
    }

    public void setTemplate(DPFPTemplate template) {
        DPFPTemplate old = this.template;
	this.template = template;
	firePropertyChange(TEMPLATE_PROPERTY, old, template);
    }
    
    MySqlConn mysql = new MySqlConn();
 
    public void guardarHuella(){
        //Obtiene los datos del template de la huella actual
        ByteArrayInputStream datosHuella = new ByteArrayInputStream(template.serialize());
        Integer tamañoHuella=template.serialize().length;

        //Pregunta el nombre de la persona a la cual corresponde dicha huella
        String nombre = JOptionPane.showInputDialog("Nombre:");
     
        try {
        //Establece los valores para la sentencia SQL
        //Connection c=con. //establece la conexion con la BD
            mysql.Connect();
            PreparedStatement guardarStmt = mysql.conn.prepareStatement("INSERT INTO shimon(idEmpleado, Shimon) values(?,?)");
            //query = "INSERT INTO somhue(huenombre, huehuella) values(?,?)";
            guardarStmt.setString(1,nombre);
            guardarStmt.setBinaryStream(2, datosHuella,tamañoHuella);
            //Ejecuta la sentencia
            guardarStmt.execute();
            guardarStmt.close();
            JOptionPane.showMessageDialog(null,"Huella Guardada Correctamente");
            mysql.desConnect();
            btnGuardar.setEnabled(false);
            btnVerificar.grabFocus();
        } 
        catch (SQLException ex) {
            //Si ocurre un error lo indica en la consola
            System.err.println("Error al guardar los datos de la huella.");
            System.err.println(ex.getMessage());
        }
        finally{
            mysql.desConnect();
        }
    }
    
    /**
* Verifica la huella digital actual contra otra en la base de datos
*/
    
public void verificarHuella(String nom) {
    try {
        //Establece los valores para la sentencia SQL
        mysql.Connect();
        //Obtiene la plantilla correspondiente a la persona indicada
        PreparedStatement verificarStmt = mysql.conn.prepareStatement("SELECT Shimon FROM Shimon WHERE idEmpleado=?");
        verificarStmt.setString(1,nom);
        ResultSet rs = verificarStmt.executeQuery();

        //Si se encuentra el nombre en la base de datos
        if (rs.next()){
            //Lee la plantilla de la base de datos
            byte templateBuffer[] = rs.getBytes("Shimon");
            //Crea una nueva plantilla a partir de la guardada en la base de datos
            DPFPTemplate referenceTemplate = DPFPGlobal.getTemplateFactory().createTemplate(templateBuffer);
            //Envia la plantilla creada al objeto contendor de Template del componente de huella digital
            setTemplate(referenceTemplate);

            // Compara las caracteriticas de la huella recientemente capturda con la
            // plantilla guardada al usuario especifico en la base de datos
            DPFPVerificationResult result = Verificador.verify(featuresverificacion, getTemplate());

            //compara las plantilas (actual vs bd)
            if (result.isVerified())
                JOptionPane.showMessageDialog(null, "Las huella capturada coinciden con la de "+nom,"Verificacion de Huella", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "No corresponde la huella con "+nom, "Verificacion de Huella", JOptionPane.ERROR_MESSAGE);

        } 
        //Si no encuentra alguna huella correspondiente al nombre lo indica con un mensaje
        else {
            EnviarError("No existe un registro de huella para "+nom);
        }
    } 
    catch (SQLException e) {
        //Si ocurre un error lo indica en la consola
        System.err.println("Error al verificar los datos de la huella.");
    }
    finally{
       mysql.desConnect();
    }
    
}
 
 /**
  * Identifica a una persona registrada por medio de su huella digital
  */
    
public void identificarHuella() throws IOException{
    try {
        //Establece los valores para la sentencia SQL
        mysql.Connect();

        //Obtiene todas las huellas de la bd
        PreparedStatement identificarStmt = mysql.conn.prepareStatement("SELECT idEmpleado, Shimon FROM shimon");
        ResultSet rs = identificarStmt.executeQuery();
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = timeFormat.format(new Date());

        //Si se encuentra el nombre en la base de datos
        while(rs.next()){
            //Lee la plantilla de la base de datos
            byte templateBuffer[] = rs.getBytes("Shimon");
            String id =rs.getString("idEmpleado");
            //Crea una nueva plantilla a partir de la guardada en la base de datos
            DPFPTemplate referenceTemplate = DPFPGlobal.getTemplateFactory().createTemplate(templateBuffer);
            //Envia la plantilla creada al objeto contendor de Template del componente de huella digital
            setTemplate(referenceTemplate);

            // Compara las caracteriticas de la huella recientemente capturda con la
            // alguna plantilla guardada en la base de datos que coincide con ese tipo
            DPFPVerificationResult result = Verificador.verify(featuresverificacion, getTemplate());

            //compara las plantilas (actual vs bd)
            //Si encuentra correspondencia dibuja el mapa
            //e indica el nombre de la persona que coincidió.
            if (result.isVerified()){
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                
                String fecha = dateFormat.format(new Date());
                
                if(!horaL.getText().equals("")){
                    try {
                        Date hora1 = timeFormat.parse(hora);
                        Date hora2 = timeFormat.parse(horaL.getText());
                        if(hora1.before(hora2)){
                            System.out.println("Yeah");
                            consumos = 0;
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                numNominaL.setText(id);
                String[] nombre = getEmployeeData(id);
                nombreL.setText(nombre[0]);
                paternoL.setText(nombre[1]);
                maternoL.setText(nombre[2]);
                String empresa = nombre[3];
                fechaL.setText(fecha);
                horaL.setText(hora);
                Image img = null;
                Image dimg = null;
                try {
                    img = ImageIO.read(new URL("http://localhost/Comedor/api/empleado/photo/"+id+".png"));
                    System.out.println(new URL("http://localhost/Comedor/api/empleado/photo/"+id+".png"));
                    dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
                    jLabel1.setIcon( new ImageIcon(dimg));
                } 
                catch (Exception e) {
                    System.out.print(e);
                    try{
                        img = ImageIO.read(new URL ("http://localhost./Comedor/api/empleado/photo/noimage.png"));
                        dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
                        jLabel1.setIcon( new ImageIcon(dimg));
                    }
                    catch(Exception e2){
                        System.out.print(e2);
                    }
                }
                if(checkConsumption(id, fecha)){
                    tipoL.setForeground(new Color(209, 27, 27));
                    tipoL.setText("DENEGADO");
                    EnviarTexto(hora+"-DENEGADO: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                    try {
                        SoundUtils.tone(440, 500, 1000);
                    } catch (LineUnavailableException ex) {
                        Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else{
                    int aux = getConsumptionType();
                    if (aux==1){
                        tipoL.setForeground(new Color(3, 119, 48));
                        tipoL.setText("LONCHE");
                        EnviarTexto(hora+"-LONCHE: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                        try {
                            SoundUtils.tone(2218,200);
                        } catch (LineUnavailableException ex) {
                            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else{
                        tipoL.setForeground(new Color(3, 119, 48));
                        tipoL.setText("COMIDA");
                        EnviarTexto(hora+"-COMIDA: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                        try {
                            SoundUtils.tone(2218,200);
                        } catch (LineUnavailableException ex) {
                            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        consumos++;
                        totalConsumosL.setText(String.valueOf(consumos));
                    }
                    
                    insertConsumpion(id, fecha, empresa);
                }
                return;
            }
        }
        //Si no encuentra alguna huella correspondiente al nombre lo indica con un mensaje
        try{
            Image img = null, dimg = null;
            img = ImageIO.read(new URL ("http://localhost./Comedor/api/empleado/photo/error.png"));
            dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
            jLabel1.setIcon( new ImageIcon(dimg));
        }
        catch(Exception e2){
            System.out.print(e2);
        }
        numNominaL.setText("ERROR");
        nombreL.setText("ERROR");
        paternoL.setText("ERROR");
        maternoL.setText("ERROR");
        EnviarError(hora+": Ningún registro coincide con la huella");
        try {
            SoundUtils.tone(440, 500, 1000);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
        setTemplate(null);
    } 
    catch (SQLException e) {
       //Si ocurre un error lo indica en la consola
       System.err.println("Error al identificar huella dactilar."+e.getMessage());
    }
    finally{
       mysql.desConnect();
    }
}

public void identificarCodigo(String barcode){
    try {
        //Conexión con BD para obtener datos de los códigos de barras.
        mysql.Connect();      
        PreparedStatement identificarStmt = mysql.conn.prepareStatement("SELECT * from empleado where idEmpleado like '" + barcode + "'");
        System.out.println(identificarStmt);
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = timeFormat.format(new Date());
        ResultSet rs = identificarStmt.executeQuery();
        
        //En caso de que el usuario ya esté registrado con su código de barras:
        if (rs.next()){
            rs.close();
            
            //Despliegue de datos en la interfaz de consumos:
            String id = barcode;
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            String fecha = dateFormat.format(new Date());
            if(!horaL.getText().equals("")){
                try {
                    Date hora1 = timeFormat.parse(hora);
                    Date hora2 = timeFormat.parse(horaL.getText());
                    if(hora1.before(hora2)){
                        System.out.println("Yeah");
                        consumos = 0;
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            numNominaL.setText(id);
            String[] nombre = getEmployeeData(id);
            nombreL.setText(nombre[0]);
            paternoL.setText(nombre[1]);
            maternoL.setText(nombre[2]);
            String empresa = nombre[3];
            fechaL.setText(fecha);
            horaL.setText(hora);
            
            Image img = null;
            Image dimg = null;
            try {
                img = ImageIO.read(new URL("http://localhost./Comedor/api/empleado/photo/"+id+".png"));
                System.out.println(new URL("http://localhost/Comedor/api/empleado/photo/"+id+".png"));
                dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
                jLabel1.setIcon( new ImageIcon(dimg));
            } 
            catch (Exception e) {
                System.out.print(e);
                try{
                    img = ImageIO.read(new URL ("http://localhost/Comedor/api/empleado/photo/noimage.png"));
                    dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
                    jLabel1.setIcon( new ImageIcon(dimg));
                }
                catch(Exception e2){
                    System.out.print(e2);
                }
            }
            
            //Si el usuario ya comió en el día, no puede comer otra vez:
            if(checkConsumption(id, fecha)){
                tipoL.setForeground(new Color(209, 27, 27));
                tipoL.setText("DENEGADO");
                EnviarTexto(hora+"-DENEGADO: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                try {
                    SoundUtils.tone(440, 500, 1000);
                } catch (LineUnavailableException ex) {
                    Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
           
            //En el caso de que no haya comido aún:
            else{
                int aux = getConsumptionType(); //Obtiene el tipo de consumo según horario.
                
                //Si es Lunch Break, registra el consumo como tal:
                if (aux==1){
                    tipoL.setForeground(new Color(3, 119, 48));
                    tipoL.setText("BREAK");
                    EnviarTexto(hora+"-BREAK: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                    try {
                        SoundUtils.tone(2218,200);
                    } catch (LineUnavailableException ex) {
                        Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                //En caso contrario, registra el consumo como comida:
                else{
                    tipoL.setForeground(new Color(3, 119, 48));
                    tipoL.setText("COMIDA");
                    EnviarTexto(hora+"-COMIDA: "+nombre[0]+" "+nombre[1]+" "+nombre[2]);
                    try {
                        SoundUtils.tone(2218,200);
                    } catch (LineUnavailableException ex) {
                        Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    consumos++;
                    totalConsumosL.setText(String.valueOf(consumos));
                }

                insertConsumpion(id, fecha, empresa); //Inserta el consumo en la BD.
            }
            return;
        }
        //Si no encuentra algún código que corresponda con el leído, lanza error a la consola:
        try{
            Image img = null, dimg = null;
            img = ImageIO.read(new URL ("http://localhost./Comedor/api/empleado/photo/error.png"));
            dimg = img.getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_SMOOTH);
            jLabel1.setIcon( new ImageIcon(dimg));
        }
        catch(Exception e2){
            System.out.print(e2);
        }
        numNominaL.setText("ERROR");
        nombreL.setText("ERROR");
        paternoL.setText("ERROR");
        maternoL.setText("ERROR");
        EnviarError(hora+" "+barcode+": Ningún registro coincide con el código.");
        try {
            SoundUtils.tone(440, 500, 1000);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
        setTemplate(null);
    } 
    catch (SQLException e) {
       //Si ocurre algún error en la BD, lo indica en la consola:
       System.err.println("Error al identificar huella dactilar."+e.getMessage());
    }
    finally{
       mysql.desConnect();
    }
}

    public String[] getEmployeeData(String id){
        PreparedStatement getNameStmt;
        String[] nombre = new String[4];
        try {
            getNameStmt = mysql.conn.prepareStatement("SELECT Nombre, Paterno, Materno, Empresa FROM empleado WHERE idEmpleado like '"+id+"'");
            ResultSet res = getNameStmt.executeQuery();
            res.next();
            nombre[0] = res.getString("Nombre");
            nombre[1] = res.getString("Paterno");
            nombre[2] = res.getString("Materno");
            nombre[3] = res.getString("Empresa");
            getNameStmt.close();

        } 
        catch (SQLException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
            nombre = null;
        } 
        return nombre;
    }

    public boolean checkConsumption(String id, String fecha){
        PreparedStatement checkStmt;
        try {
            checkStmt = mysql.conn.prepareStatement("SELECT * FROM Consumo WHERE idEmpleado like '" + id + "' and date(Fecha) like '" + fecha + "' and tipo = b'0'");
            ResultSet resCheck = checkStmt.executeQuery();
            if(resCheck.next() && getConsumptionType()==0){
                checkStmt.close();
                return true;
            }
            else{
                checkStmt.close();
                return false;
            }
        } 
        catch (SQLException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean insertConsumpion(String id, String fecha, String empresa){
        float precio = getCost(); 
        PreparedStatement getCost;
        try {
            String aux = "INSERT INTO consumo(idEmpleado, Fecha, Precio, Tipo, Empresa) values('"+id+"', NOW(), "+precio+", b'"+getConsumptionType()+", "+empresa+"')";
            getCost = mysql.conn.prepareStatement(aux);
            System.out.println(getCost);
            getCost.execute();
            getCost.close();
            return true;
        } 
        catch (SQLException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public int getConsumptionType(){
        PreparedStatement checkStmt;
        try {
            checkStmt = mysql.conn.prepareStatement("select * from horario where Inicio < TIME(NOW()) and Fin > TIME(NOW())");
            ResultSet resCheck = checkStmt.executeQuery();
            if(resCheck.next()){
                return resCheck.getInt("Tipo");
            }
            else{
                checkStmt.close();
                return 0;
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public float getCost(){
        PreparedStatement checkStmt;
        try {
            checkStmt = mysql.conn.prepareStatement("select * from precio order by Fecha desc limit 1");
            ResultSet resCheck = checkStmt.executeQuery();
            resCheck.next();
            float precio = resCheck.getFloat("Precio");
            resCheck.close();
            if(precio > 0 && getConsumptionType() == 0){
                return precio;
            }
            else{
                checkStmt.close();
                return 0;
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblImagenHuella = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        totalConsumosL = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        fechaL = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        horaL = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        numNominaL = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nombreL = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        paternoL = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        maternoL = new javax.swing.JLabel();
        btnVerificar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnIdentificar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        tipoL = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Huellas Dactilares - J@RC 2011 4500 Fingerprint Reader DP");
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(366, 90));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Huella"));

        lblImagenHuella.setBackground(new java.awt.Color(255, 255, 255));
        lblImagenHuella.setText("huella");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jScrollPane2.setViewportView(jTextPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addComponent(lblImagenHuella, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblImagenHuella, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        jPanel2.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 530, 610, 170));

        jLabel5.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel5.setText("Consumos:");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 470, -1, -1));

        totalConsumosL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        totalConsumosL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalConsumosL.setText("0");
        jPanel2.add(totalConsumosL, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 470, 329, -1));

        jLabel7.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel7.setText("Fecha:");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 350, -1, -1));

        fechaL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        fechaL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(fechaL, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 350, 420, -1));

        jLabel9.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel9.setText("Hora:");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 390, -1, -1));

        horaL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        horaL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(horaL, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 390, 200, -1));

        jLabel3.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel3.setText("Número de Nómina: ");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 190, -1, -1));

        numNominaL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        numNominaL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(numNominaL, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 190, 180, -1));

        jLabel2.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel2.setText("Nombre:");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 230, -1, -1));

        nombreL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        nombreL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(nombreL, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 230, 290, -1));

        jLabel6.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel6.setText("Ap. Paterno:");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 270, -1, -1));

        paternoL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        paternoL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(paternoL, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 270, 310, -1));

        jLabel8.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel8.setText("Ap. Materno:");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 310, -1, -1));

        maternoL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        maternoL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(maternoL, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 310, 310, -1));

        btnVerificar.setText("Verificar");
        btnVerificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarActionPerformed(evt);
            }
        });
        jPanel2.add(btnVerificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 720, 139, 33));

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel2.add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 720, 128, 32));

        btnIdentificar.setText("Identificar");
        btnIdentificar.setPreferredSize(new java.awt.Dimension(71, 23));
        btnIdentificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIdentificarActionPerformed(evt);
            }
        });
        jPanel2.add(btnIdentificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 720, 139, 30));

        jLabel4.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel4.setText("Consumo:");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 430, -1, -1));

        tipoL.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        tipoL.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPanel2.add(tipoL, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 430, 370, -1));
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, 90, 90));

        jLabel11.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(11, 49, 143));
        jLabel11.setText("SUMITOMO ELECTRIC");
        jPanel2.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, -1, -1));

        jLabel12.setFont(new java.awt.Font("Montserrat Semi Bold", 0, 36)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(11, 49, 143));
        jLabel12.setText("SINTERED COMPONENTS MÉXICO");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 117, -1, 40));

        jLabel1.setText("jLabel1");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 200, 390, 480));

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(1366, 768));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        Iniciar();
	start();
        EstadoHuellas();
        btnGuardar.setEnabled(false);
        btnIdentificar.setEnabled(false);
        btnVerificar.setEnabled(false);
        
        try{
            URL url = new URL("http://localhost/Comedor/pics/logo.jpg");
            BufferedImage bfim = ImageIO.read(url);
            jLabel10.setIcon(new ImageIcon(bfim));
        }
        catch(Exception e){
            System.err.println(e);
        }      
        this.setFocusable(true);
        this.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        stop();
    }//GEN-LAST:event_formWindowClosing

    private void btnIdentificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIdentificarActionPerformed
        try {
            identificarHuella();
            Reclutador.clear();
        } catch (IOException ex) {
            Logger.getLogger(CapturaHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnIdentificarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        guardarHuella();
        Reclutador.clear();
        lblImagenHuella.setIcon(null);
        start();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnVerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarActionPerformed
        String nombre = JOptionPane.showInputDialog("Nombre a verificar:");
        verificarHuella(nombre);
        Reclutador.clear();
    }//GEN-LAST:event_btnVerificarActionPerformed

/*
* @param args the command line arguments
*/
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CapturaHuella().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnIdentificar;
    private javax.swing.JButton btnVerificar;
    private javax.swing.JLabel fechaL;
    private javax.swing.JLabel horaL;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JLabel lblImagenHuella;
    private javax.swing.JLabel maternoL;
    private javax.swing.JLabel nombreL;
    private javax.swing.JLabel numNominaL;
    private javax.swing.JLabel paternoL;
    private javax.swing.JLabel tipoL;
    private javax.swing.JLabel totalConsumosL;
    // End of variables declaration//GEN-END:variables
    
}
