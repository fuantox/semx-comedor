<?php

    /*
    require 'template.php';
    */


    function head()
    {
    ?>
        <head>
        <!--Tipo de contenido y lenguaje-->
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Language" content="es-mx" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--Hojas de estilo-->
            <link type="text/css" rel="stylesheet" href="css/theme.css"  media="screen,projection"/>
            <link href="css/style.css" rel="stylesheet">
            <link rel="shortcut icon" href="favicon.ico">
            <link rel="stylesheet" href="css/font-awesome.css">
            <link rel="stylesheet" href="css/sweetalert.css">
            <link rel="stylesheet" href="css/colors.css">
        <!--Scripts externos-->  
        <!--Título de la Página (barra de títulos del navegador)-->
            <title>SEMXSys - Dining Room</title>
        </head>
    <?php
    }

    function navbar()
    {
        $carrito = 0;
        if(isset($_SESSION["carrito"])){
            $carrito = $_SESSION["carrito"];
        }
    ?>
        <div class="navbar navbar-fixed-top navbar-default sumi-blue">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1 class="logo">
                        
                        <a href="home.php"><img class="logo-sym" src="pics/sumisym.svg">SUMITOMO ELECTRIC SINTERED<br>COMPONENTS MÉXICO</a>
                    </h1>
                </div>
            
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                                            
                        <?php if(!isset($_SESSION["id"])){ ?>
                            <li class="active navbar-links"><a href="login.php">Sign In<span class="sr-only">(current)</span></a></li>
                        <?php }else{ ?>
                            <li class="active navbar-links"><a href="employees.php">Employees<span class="sr-only">(current)</span></a></li>
                            <li class="active navbar-links"><a href="times.php">Meal Times<span class="sr-only">(current)</span></a></li>
                            <li class="active navbar-links"><a href="price.php">Meal Price<span class="sr-only">(current)</span></a></li>
                            <li class="active navbar-links"><a href="report.php">Report<span class="sr-only">(current)</span></a></li>
                            <li class="active navbar-links"><a href="endSession.php">Sign Out<span class="sr-only">(current)</span></a></li>
                        <?php }?>
                    </ul>
                </div>
            </div> 
        </div>
    <?php
    }

    function footer()
    {
        ?>
        <footer class="footer">
            &copy; SUMITOMO ELECTRIC SINTERED COMPONENTS MÉXICO S.A. DE C.V.
        </footer>
        <?php
    }

    function stickyFooter()
    {
        ?>
        <footer class="footer sticky-footer">
            &copy; SUMITOMO ELECTRIC SINTERED COMPONENTS MÉXICO S.A. DE C.V.
        </footer>
        <?php
    }

    function scripts()
    {
        ?>
        <script src="js/jquery-2.1.4.js"></script>
        <script type="text/javascript" src="js/tether.min.js"></script>  <!-- Resource jQuery -->
        <script type="text/javascript" src="js/bootstrap3.js"></script>  <!-- Resource jQuery -->
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <script type="text/javascript" src="js/sweetalert.min.js"></script>  <!-- Resource jQuery -->
        <?php
    }