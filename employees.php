<?php
    require 'template.php';
    session_start();
    if(!isset($_SESSION["id"])){
        header("Location: login.php");
    }
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-datepicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
        ?>
        
        <div class="main-content">
            <div class="container">
                <div class="row searchDiv">
                    <div class="col-md-3">
                        <h1>Employees</h1>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group searchbar" id="searchBar">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                            <input type="text" id="searchBarInput" class="form-control" placeholder="Search an employee..." aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary pull-right searchbar" id="addEmployeeBtn" onclick="showAddForm()">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            &nbsp;Add Employee
                        </a>
                    </div>                
                </div>
            </div>
                        
            <div class="content grey lighten-3" id="addEmpForm">
                <form   id="addEmployeeForm">
                    <div class="container" id="addEmpFormContainer" >
                        <h3>Add an employee</h3>
                        Please introduce all the data of the new employee.
                        <br><br>
                        
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row addEmployeeRow">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="ID" name="idEmpleado" id="idEmpleado" title="Introduce only numbers."/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="Ingreso" placeholder="Employment Date"/>
                                    </div>
                                </div>
                                <div class="row addEmployeeRow">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Name" name="Nombre" id="Nombre" title="Introduce only letters."/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Primary Last Name" name="Paterno" id="Paterno" title="Introduce only letters."/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Secondary Last Name" name="Materno" id="Materno" title="Introduce only letters."/>
                                    </div>
                                </div>
                                <div class="row addEmployeeRow">
                                    <div class="col-md-4">
                                        <select class="form-control" name="Empresa" id="Empresa">
                                          <option disabled selected hidden>Company</option>
                                          <option value="Sumitomo">Sumitomo</option>
                                          <option value="Capital Humano">Capital Humano</option>
                                          <option value="Intern">Internship</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" name="Area" id="Area">
                                            <option disabled selected hidden>Cost Center</option>
                                            <option value="5011 Manufacturing">5011 Manufacturing</option>
                                            <option value="5012 Quality AssuranceX">5012 Quality AssuranceX</option>
                                            <option value="5021 Production">5021 Production</option>
                                            <option value="5022 Facility Maintenance">5022 Facility Maintenance</option>
                                            <option value="5023 Engineering">5023 Engineering</option>
                                            <option value="5024 Quality Assurance">5024 Quality Assurance</option>
                                            <option value="5032 Compacting">5032 Compacting</option>
                                            <option value="5033 Sintering">5033 Sintering</option>
                                            <option value="5034 Sizing">5034 Sizing</option>
                                            <option value="5035 Machining">5035 Machining</option>
                                            <option value="5036 Maintenance">5036 Maintenance</option>
                                            <option value="5037 Tooling">5037 Tooling</option>
                                            <option value="5038 Sorting">5038 Sorting</option>
                                            <option value="5039 Inspection">5039 Inspection</option>
                                            <option value="5040 Shipping">5040 Shipping</option>
                                            <option value="5041 Magnaflux">5041 Magnaflux</option>
                                            <option value="6100 Sales">6100 Sales</option>
                                            <option value="6200 Administration">6200 Administration</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" name="Nomina" id="Nomina">
                                          <option disabled selected hidden>Payroll Type</option>
                                          <option value="Semanal">Weekly</option>
                                          <option value="Quincenal">Two-Week Payment</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row addEmployeeRow">
                                    <div class="col-md-12">
                                        <label class="btn btn-default btn-file btn-block preview" id="photoBtn">
                                            <div id="image_preview">
                                                <i class="fa fa-camera" aria-hidden="true"></i>&nbsp;<span id="hasError">Select Photo...</span>
                                            </div>
                                            <input type="file" class="hidden" accept="image/*" name="file" id="inputPhoto">
                                        </label>
                                    </div>
                                </div>
                                <div class="row addEmployeeRow">
                                    <div class="col-md-12">
                                        <a class="btn btn-success btn-block" onclick="addEmployee()">
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                            &nbsp;Add Employee
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        
                    </div>
                </form>
            </div>
            
            <div class="container listChartContainer" id="listBody">
                
            </div>
            
        
            <div class="modal fade" tabindex="-1" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title montserrat-SB" id="profileTitle">Profile - XXXXXX</h3>
                        </div>
                        <div class="modal-body" id="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-3">
                                    <img id="previewimg" src="api/empleado/photo/0.png" style="width: 100%;">
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-6">
                                    <h4><span class="montserrat-SB">ID:</span> <span id="pf-id">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Name: </span> <span id="pf-name">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Primary Last Name: </span> <span id="pf-prim">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Secondary Last Name: </span> <span id="pf-secon">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Company: </span> <span id="pf-comp">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Cost Center: </span> <span id="pf-cost">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Payroll Type: </span> <span id="pf-pay">XXXXXXXX</span></h4>
                                    <h4><span class="montserrat-SB">Employment Date: </span> <span id="pf-emp">XXXXXXXX</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary hidden" aria-label="Edit" onclick="passToSumi()" id="passB">
                                <img class="icon-sumi" src="pics/sumisym.svg"></img>
                                &nbsp;Pass to Sumitomo
                            </button>
                            <button type="button" class="btn btn-info" aria-label="Edit" onclick="editUser()" id="editB">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                &nbsp;Edit
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                Close
                            </button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>

            
            
        </div>
        
        
        <?php
            footer();
        ?>

    </body> 
        
<?php
    scripts();
?>
    <script type="text/javascript" src="js/employees.js"></script>
    <script type="text/javascript" src="js/photo.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
    <script>
        $('#Ingreso').datepicker({
            format: "yyyy-mm-dd",
            language: "en",
            autoclose: true,
            todayHighlight: true
        });
        $('#myModal').on('hidden.bs.modal', function () {
            document.getElementById("editB").innerHTML = '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit';
            document.getElementById("editB").onclick = editUser;
        })
    </script>

</html>