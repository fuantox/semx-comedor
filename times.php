<?php
    require 'template.php';
    session_start();
    if(!isset($_SESSION["id"])){
        header("Location: login.php");
    }
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-clockpicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
            navbar();
        ?>
        
        <div class="main-content">
            <div class="container">
                <div class="row searchDiv">
                    <div class="col-md-9">
                        <h1>Lunch Time Schedule</h1>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary pull-right searchbar" id="addTimeBtn" data-toggle="modal" data-target="#myModal" onclick="showAddForm()">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            &nbsp;Add Meal Time
                        </a>
                    </div>                
                </div>
            </div>
            
            <div class="content grey lighten-3" id="addTimeForm">
                <form id="addMealForm">
                    <div class="container" id="addMealFormContainer" >
                        <h3>Add a Meal Time</h3>
                        Please introduce all the data of the new meal time.
                        <br><br>
                        <div class="row addEmployeeRow">
                            <div class="col-md-3 clockpicker">
                                <input type="text" class="form-control" id="start" name="start" placeholder="Start Time">
                            </div>
                            <div class="col-md-3 clockpicker">
                                <input type="text" class="form-control" id="finish" name="finish" placeholder="End Time">
                            </div>
                    
                            <div class="col-md-3">
                                <select class="form-control" name="type" id="type">
                                    <option disabled selected hidden>Meal Type</option>
                                    <option value="0">Lunch</option>
                                    <option value="1">Break</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-success btn-block" onclick="addTime()">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;Add Lunch Time
                                </a>
                            </div>     
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="container listChartContainer" id="listBody">
                
            </div>
            
        </div>
        
        
        
        <?php
            footer();
        ?>

    </body> 
        
<?php
    scripts();
?>
    <script type="text/javascript" src="js/times.js"></script>
    <script type="text/javascript" src="js/bootstrap-clockpicker.js"></script>
    <script type="text/javascript">
        $('.clockpicker').clockpicker({
            autoclose: true,
            default: 'now',
            placement: 'right'
        });
    </script>

</html>