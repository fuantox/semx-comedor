<?php
    require 'template.php';
    session_start();
    if(!isset($_SESSION["id"])){
        header("Location: login.php");
    }
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-clockpicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
            navbar();
        ?>
        
        <div class="main-content">
            <div class="container">
                <div class="row searchDiv">
                    <div class="col-md-9">
                        <h1>Meal Pricing Settings</h1>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary pull-right searchbar" id="addTimeBtn" data-toggle="modal" data-target="#myModal" onclick="showAddForm()">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            &nbsp;Edit Meal Price
                        </a>
                    </div>                
                </div>
            </div>
            
            <div class="content grey lighten-3" id="addTimeForm">
                <form id="addMealForm">
                    <div class="container" id="addMealFormContainer" >
                        <h3>Change the Meal Price</h3>
                        Please introduce the necesary data for changing the meal price.
                        <br><br>
                        <div class="row addEmployeeRow">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" id="precio" name="precio" placeholder="New Price">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="razon" name="razon" placeholder="Reason"/>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-success btn-block" onclick="addPrice()">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    &nbsp;Confirm New Price
                                </a>
                            </div>     
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="container listChartContainer" id="listBody">
                
            </div>
            
        </div>
        
        
        
        <?php
            footer();
        ?>

    </body> 
        
<?php
    scripts();
?>
    <script type="text/javascript" src="js/price.js"></script>

</html>