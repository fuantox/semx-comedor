$(document).ready(function (e) {    
    $("#inputPhoto").change(function() {
        $('#image_preview').html('<div id="image_preview" class="red-text"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;Loading...</div>');
        var form = $('form')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        formData.append('image', $("#inputPhoto")[0].files[0]); 
        $.ajax({
            url: "API/ajax_photo.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                console.log(data);
            }
        });
    });
    
    // Function to preview image after validation
    $(function() {
        $("#inputPhoto").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
                $('#image_preview').html('<div id="image_preview" class="red-text"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;<span id="hasError">Error!</span></div>');
                return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    
    function imageIsLoaded(e) {
        $('#image_preview').css("display", "block");
        $('#image_preview').html('<div id="image_preview"><img id="previewing" src="API/empleado/photo/noimage.png" /></div>');
        $('#previewing').attr('src', e.target.result);
        $('#photoBtn').css('padding-top', '5px');
        $('#photoBtn').css('padding-bottom', '5px');
        $('#previewing').attr('height', '101px');
        $('#previewing').css('max-width', '230px');
    };
});