function showAddForm(){
    form = document.getElementById("addTimeForm");
    button = document.getElementById("addTimeBtn");
    if(form.style.display != "block"){
        form.style.display = "block";
        button.innerHTML = "Close&nbsp;<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        button.className = "btn btn-secondary pull-right searchbar";
    }
    else{
        form.style.display = "none";
        button.className = "btn btn-primary pull-right searchbar";
        button.innerHTML = "<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;Add Meal Time";
        document.getElementById("searchBarInput").focus();
    }
};

function addTime() { 
    start = document.getElementById("start").value;
    finish = document.getElementById("finish").value;
    type = document.getElementById("type").value;
    
    
    if(start==""||finish==""||type==null){
        sweetAlert("Error", "Please fill all the fields.", "error");
    }
    else{
        params = "Inicio="+start+"&Fin="+finish+"&Tipo="+type;
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "API/horario/horario.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && http.status == 200) {
                sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
            }
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                swal("Meal Time Added", "Meal time added successfully.", "success"); 
                $('#addMealForm')[0].reset();
                showAddForm();
                showList();  
            }
        }
        xmlhttp.send(params); 
        showList();  
        showList(); 
    }
    showList();
};

function deleteTime(id){
    swal({   
        title: "Are you sure?",   
        text: "This lunch time will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/horario/"+id, true);
        xmlhttp.send();
        showList();
        swal("Deleted!", "The meal time "+id+" has been deleted.", "success"); 
        showList();
    });
    showList();
};


function showList(){
    taskBody = document.getElementById("listBody");

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            var out = "";
            var i;
            if(myArr.length!=0){

                out += '<table class="table table-sm table-hover">'
                    +       '<thead class="thead-inverse">'
                    +           '<tr>'
                    +               '<th class="table-bordered text-center sTimeColumn">Start Time</th>'
                    +               '<th class="table-bordered text-center fTimeColumn">Finish Time</th>'
                    +               '<th class="table-bordered text-center fTimeColumn">Meal Type</th>'
                    +               '<th class="table-bordered text-center optColumn hidden-print">Options</th>'
                    +           '</tr>'
                    +       '</thead>'
                    +   '<tbody id="timesTable">';

                for(i = 0; i < myArr.length; i++) {
                    out += '<tr id="tr' + myArr[i].idHorario + '">'
                    +           '<td class="text-center">' + myArr[i].Inicio + '</td>'
                    +           '<td class="text-center">' + myArr[i].Fin + '</td>';
                    
                    if(myArr[i].Tipo == 0){
                        out +=      '<td class="text-center">Lunch</td>'    
                    }
                    else{
                        out +=      '<td class="text-center">Break</td>'     
                    }
                    
                    out +=      '<td class="text-center hidden-print">'
                    +               '<a onclick="editTime(' + myArr[i].idHorario + ')" class="btn btn-info btn-sm" href="#" aria-label="Settings">'
                    +                   '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Edit'
                    +               '</a>&nbsp;'
                    +               '<a onclick="deleteTime(' + myArr[i].idHorario + ')" href="#" class="btn btn-danger btn-sm">'
                    +                   '<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;&nbsp;Delete'
                    +               '</a>&nbsp;'
                    +           '</td>'
                    +       '</tr>';     
                }


            out += '</tbody>'
                    +'</table>';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No meal schedule found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
    urlPath = "API/horario/";
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

showList();