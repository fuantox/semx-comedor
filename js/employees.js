function showAddForm(){
    form = document.getElementById("addEmpForm");
    button = document.getElementById("addEmployeeBtn");
    if(form.style.display != "block"){
        form.style.display = "block";
        button.innerHTML = "Close&nbsp;<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        button.className = "btn btn-secondary pull-right searchbar";
    }
    else{
        form.style.display = "none";
        button.className = "btn btn-primary pull-right searchbar";
        button.innerHTML = "<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;Add Employee";
        document.getElementById("searchBarInput").focus();
    }
};

function addEmployee() { 
    idEmpleado = document.getElementById("idEmpleado").value;
    nombre = document.getElementById("Nombre").value;
    paterno = document.getElementById("Paterno").value;
    materno = document.getElementById("Materno").value;
    empresa = document.getElementById("Empresa").value;
    area = document.getElementById("Area").value;
    nomina = document.getElementById("Nomina").value;
    ingreso = document.getElementById("Ingreso").value;
    
    params = "idEmpleado="+idEmpleado+"&Nombre="+nombre+"&Paterno="+paterno+"&Materno="+materno
        +"&Empresa="+empresa+"&Area="+area+"&Nomina="+nomina+"&Ingreso="+ingreso;
    foto = $("#inputPhoto").val();
    if(idEmpleado==""||nombre==""||paterno==""||ingreso==""||empresa==null||area==null||nomina==null){
        sweetAlert("Error", "Please fill all the fields.", "error");
    }
    else if(foto == ""||$("#hasError").val() == "Select Photo..."||$("#hasError").val() == "Error!"){
        sweetAlert("Error", "Please select a valid photo.", "error");
    }
    else{
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "API/empleado/empleado.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && http.status == 200) {
                sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
            }
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                response = JSON.parse()
                swal("Employee Added", "Employee "+idEmpleado+" added successfully.", "success"); 
                $('#addEmployeeForm')[0].reset();
                showAddForm();
                showList();  
            }
        }
        xmlhttp.send(params); 
        showList();  
        showList(); 
    }
    showList();
    $('#photoBtn').css('padding-top', '45px');
    $('#photoBtn').css('padding-bottom', '46px');
    $('#image_preview').html('<div id="image_preview"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;<span id="hasError">Select Photo...</span></div>');
};

function deleteEmployee(id){
    swal({   
        title: "Are you sure?",   
        text: "All employee data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/empleado/"+id, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Employee "+id+" has been deleted.", "success"); 
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();
};

function getProfile(id){
    xmlhttp2 = new XMLHttpRequest();
    urlPath = "API/empleado/" + id;
    console.log(urlPath);
    xmlhttp2.open("get", urlPath, true);

    xmlhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            if(myArr.length!=0){
                //console.log(xmlhttp2.responseText);
                document.getElementById("profileTitle").innerHTML = "Profile - " + myArr[0].idEmpleado;
                document.getElementById("pf-id").innerHTML = myArr[0].idEmpleado;
                document.getElementById("pf-name").innerHTML = myArr[0].Nombre;
                document.getElementById("pf-prim").innerHTML = myArr[0].Paterno;
                document.getElementById("pf-secon").innerHTML = myArr[0].Materno;
                document.getElementById("pf-comp").innerHTML = myArr[0].Empresa;
                document.getElementById("pf-cost").innerHTML = myArr[0].Area;
                document.getElementById("pf-pay").innerHTML = myArr[0].Nomina;
                document.getElementById("pf-emp").innerHTML = myArr[0].Ingreso;
                document.getElementById("previewimg").src = "api/empleado/photo/" + id + ".png";
                var img = document.getElementById("previewimg");
                img.onerror = function () { 
                    document.getElementById("previewimg").src = "api/empleado/photo/noimage.png";
                }
                var regex = /.*Sumitomo.*/
                if(!myArr[0].Empresa.match(regex)){
                    $('#passB').removeClass('hidden');
                }
                $('#myModal').modal('toggle');
            }
            else{
                console.log(xmlhttp2.responseText);
            }
        }
    }
    
    xmlhttp2.send();
    showList();
};


function showList(){
    taskBody = document.getElementById("listBody");

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            var out = "";
            var i;
            if(myArr.length!=0){

                out += '<table class="table table-sm table-hover">'
                    +       '<thead class="thead-inverse">'
                    +           '<tr>'
                    +               '<th class="table-bordered text-center idColumn">ID</th>'
                    +               '<th class="table-bordered text-center nameColumn">Name</th>'
                    +               '<th class="table-bordered text-center plColumn">Primary Last Name</th>'
                    +               '<th class="table-bordered text-center slColumn">Second Last Name</th>'
                    +               '<th class="table-bordered text-center comColumn">Company</th>'
                    +               '<th class="table-bordered text-center optColumn hidden-print">Options</th>'
                    +           '</tr>'
                    +       '</thead>'
                    +   '<tbody id="taskTable">';

                for(i = 0; i < myArr.length; i++) {
                out += '<tr id="tr' + myArr[i].idEmpleado + '">'
                    + '<td id="title' + myArr[i].idEmpleado + '" class="text-center">' + myArr[i].idEmpleado + '</td>'
                    + '<td id="newTitle' + myArr[i].idEmpleado + '" class="form-inline text-center" style="display: none">'
                    +       '<input class="newTitleInput form-control" placeholder="Introduce new title" value="' + myArr[i].Nombre + '" id="inputTitle'+myArr[i].idActividad+'"/>&nbsp;'
                    +       '<a  href="#" onclick="updateTitle(' + myArr[i].idEmpleado + ')" class="btn btn-success btn-sm">'
                    +           '<i class="fa fa-check" aria-hidden="true"></i>'
                    +       '</a>&nbsp;'    
                    +       '<a  href="#" onclick="dismissTitle(' + myArr[i].idEmpleado + ')" class="btn btn-danger btn-sm">'
                    +           '<i class="fa fa-times" aria-hidden="true"></i>'
                    +       '</a>&nbsp;' 
                    + '</td>'

                    + '<td id="status' + myArr[i].idEmpleado + '">' + myArr[i].Nombre + '</td>'

                    + '<td id="newStatus' + myArr[i].idEmpleado + '" class="form-inline text-center" style="display: none">'
                    +       '<input class="statusInput form-control" placeholder="Introduce new status" id="input'+myArr[i].idEmpleado+'"/>&nbsp;'
                    +       '<a  href="#" onclick="updateStatus(' + myArr[i].idEmpleado + ')" class="btn btn-success btn-sm">'
                    +           '<i class="fa fa-check" aria-hidden="true"></i>'
                    +       '</a>&nbsp;'    
                    +       '<a  href="#" onclick="dismiss(' + myArr[i].idEmpleado + ')" class="btn btn-danger btn-sm">'
                    +           '<i class="fa fa-times" aria-hidden="true"></i>'
                    +       '</a>&nbsp;' 
                    + '</td>'
                    + '<td>' + myArr[i].Paterno + '</td>'
                    + '<td>' + (myArr[i].Materno==null ? '' : myArr[i].Materno) + '</td>'
                    + '<td class="text-center">' + myArr[i].Empresa + '</td>'
                    + '<td class="text-center hidden-print">'
                    +   '<a onclick="getProfile(\'' + myArr[i].idEmpleado + '\')" class="btn btn-info btn-sm" href="#" aria-label="Settings">'
                    +       '<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;Profile'
                    +   '</a>&nbsp;'
                    +   '<a onclick="deleteEmployee(\'' + myArr[i].idEmpleado + '\')" href="#" class="btn btn-danger btn-sm">'
                    +       '<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;&nbsp;Delete'
                    +   '</a>&nbsp;'
                    + '</td>'
                    +'</td>';     
                }


            out += '</tbody>'
                    +'</table>';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No employees found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
    urlPath = "API/empleado/" + document.getElementById("searchBarInput").value;
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function passToSumi(){
    oldId = document.getElementById("pf-id").innerHTML;
    name = document.getElementById("pf-name").innerHTML;
    $('#myModal').modal('toggle');
    swal({
        title: "New ID Number",
        text: "Please introduce the new ID Number for " + name + ".",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "New ID Number..."
    },
    function(inputValue){
        if (inputValue === false) return false;

        if (inputValue === "") {
            swal.showInputError("Please write the new ID number.");
            return false
        }

        params = "/"+oldId+"/"+inputValue;
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("PATCH", "API/empleado/empleado"+params, true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && http.status == 200) {
                sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
            }
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                obj = JSON.parse(xmlhttp.responseText);
                if(obj.result==="success"){
                    console.log(obj.result);
                    swal("Changes Saved", "Employee "+inputValue+" saved successfully.", "success"); 
                }else{
                    console.log(obj.result);
                    swal("Error", "Error in request. Please contact SEMX IT.", "error"); 
                }
                showList();
            }
        }
        xmlhttp.send(params);
        showList();
        $('#photoBtn').css('padding-top', '45px');
        $('#photoBtn').css('padding-bottom', '46px');
        $('#image_preview').html('<div id="image_preview"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;<span id="hasError">Select Photo...</span></div>');
        swal("Nice!", "You wrote: " + inputValue, "success");
    });
};

function editUser(){
    company = document.getElementById("pf-comp").innerHTML;
    costCenter = document.getElementById("pf-cost").innerHTML;
    paymentRole = document.getElementById("pf-pay").innerHTML;
    newName = '<input type="text" class="form-control editForm" placeholder="Name" name="pf-Nombre" id="pf-Nombre" value="' + document.getElementById("pf-name").innerHTML + '" title="Introduce only letters."/>';
    newPrim = '<input type="text" class="form-control editForm" placeholder="Primary Last Name" name="pf-Paterno" id="pf-Paterno" value="' + document.getElementById("pf-prim").innerHTML + '" title="Introduce only letters."/>';
    newSecon = '<input type="text" class="form-control editForm" placeholder="Secondary Last Name" name="pf-Materno" id="pf-Materno" value="' + document.getElementById("pf-secon").innerHTML + '" title="Introduce only letters."/>';
    newComp = '<select class="form-control editForm" name="pf-Empresa" id="pf-Empresa"><option disabled selected hidden>Company</option><option value="Sumitomo">Sumitomo</option><option value="Capital Humano">Capital Humano</option><option value="Intern">Internship</option></select>';
    newArea = '<select class="form-control editForm" name="pf-Area" id="pf-Area"> <option disabled selected hidden>Cost Center</option> <option value="5011 Manufacturing">5011 Manufacturing</option> <option value="5012 Quality AssuranceX">5012 Quality AssuranceX</option> <option value="5021 Production">5021 Production</option> <option value="5022 Facility Maintenance">5022 Facility Maintenance</option> <option value="5023 Engineering">5023 Engineering</option> <option value="5024 Quality Assurance">5024 Quality Assurance</option> <option value="5032 Compacting">5032 Compacting</option> <option value="5033 Sintering">5033 Sintering</option> <option value="5034 Sizing">5034 Sizing</option> <option value="5035 Machining">5035 Machining</option> <option value="5036 Maintenance">5036 Maintenance</option> <option value="5037 Tooling">5037 Tooling</option> <option value="5038 Sorting">5038 Sorting</option> <option value="5039 Inspection">5039 Inspection</option> <option value="5040 Shipping">5040 Shipping</option> <option value="5041 Magnaflux">5041 Magnaflux</option> <option value="6100 Sales">6100 Sales</option> <option value="6200 Administration">6200 Administration</option> </select>';
    newPay = '<select class="form-control editForm" name="pf-Nomina" id="pf-Nomina"> <option disabled selected hidden>Payroll Type</option> <option value="Semanal">Weekly</option> <option value="Quincenal">Two-Week Payment</option> </select>';
    newEmp = '<input type="text" class="form-control editForm" id="pf-Ingreso" value="' + document.getElementById("pf-emp").innerHTML + '"  placeholder="Employment Date"/>';
    document.getElementById("pf-name").innerHTML = newName;
    document.getElementById("pf-prim").innerHTML = newPrim;
    document.getElementById("pf-secon").innerHTML = newSecon;
    document.getElementById("pf-comp").innerHTML = newComp;
    document.getElementById("pf-cost").innerHTML = newArea;
    document.getElementById("pf-pay").innerHTML = newPay;
    document.getElementById("pf-emp").innerHTML = newEmp;
    document.getElementById("pf-Empresa").value  = company;
    document.getElementById("pf-Area").value  = costCenter;
    document.getElementById("pf-Nomina").value  = paymentRole;
    
    $('#pf-Ingreso').datepicker({
        format: "yyyy-mm-dd",
        language: "en",
        autoclose: true,
        todayHighlight: true, 
        container: '#modal-body'
    });
    document.getElementById("editB").innerHTML = '<i class="fa fa-check" aria-hidden="true"></i>&nbsp;Save Changes';
    
    document.getElementById("editB").onclick = function(){ 
        idEmpleado = document.getElementById("pf-id").innerHTML;
        nombre = document.getElementById("pf-Nombre").value;
        paterno = document.getElementById("pf-Paterno").value;
        materno = document.getElementById("pf-Materno").value;
        empresa = document.getElementById("pf-Empresa").value;
        area = document.getElementById("pf-Area").value;
        nomina = document.getElementById("pf-Nomina").value;
        ingreso = document.getElementById("pf-Ingreso").value;

        params = "/"+idEmpleado+"/"+nombre+"/"+paterno+"/"+materno+"/"+empresa+"/"+area+"/"+nomina+"/"+ingreso;
        if(idEmpleado==""||nombre==""||paterno==""||ingreso==""||empresa==null||area==null||nomina==null){
            sweetAlert("Error", "Please fill all the fields.", "error");
        }
        else{
            var xmlhttp;
            xmlhttp = new XMLHttpRequest();
            xmlhttp.open("PUT", "API/empleado/empleado"+params, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
                if(xmlhttp.readyState == 4 && http.status == 200) {
                    sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
                }
            }
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                    console.log(xmlhttp.responseText);
                    swal("Changes Saved", "Employee "+idEmpleado+" saved successfully.", "success"); 
                    $('#myModal').modal('toggle');
                    showList();  
                }
            }
            xmlhttp.send(params); 
            showList();  
            showList(); 
        }
        showList();
        $('#photoBtn').css('padding-top', '45px');
        $('#photoBtn').css('padding-bottom', '46px');
        $('#image_preview').html('<div id="image_preview"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;<span id="hasError">Select Photo...</span></div>');
    };
};

showList();
searchBar = document.getElementById("searchBarInput");
searchBar.addEventListener('input', showList);