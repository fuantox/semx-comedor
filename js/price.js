function showAddForm(){
    form = document.getElementById("addTimeForm");
    button = document.getElementById("addTimeBtn");
    if(form.style.display != "block"){
        form.style.display = "block";
        button.innerHTML = "Close&nbsp;<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        button.className = "btn btn-secondary pull-right searchbar";
    }
    else{
        form.style.display = "none";
        button.className = "btn btn-primary pull-right searchbar";
        button.innerHTML = '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit Meal Price';
        document.getElementById("searchBarInput").focus();
    }
};

function addPrice() { 
    precio = document.getElementById("precio").value;
    razon = document.getElementById("razon").value;    
    
    if(precio==""||razon==""){
        sweetAlert("Error", "Please fill all the fields.", "error");
    }
    else{
        params = "Precio="+precio+"&Razon="+razon;
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "API/precio/precio.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && http.status == 200) {
                sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
            }
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                swal("Meal Price Changed", "Meal price has been changed successfully.", "success"); 
                $('#addMealForm')[0].reset();
                showAddForm();
                showList();  
            }
        }
        xmlhttp.send(params); 
        showList();  
        showList(); 
    }
    showList();
};

function deleteTime(id){
    swal({   
        title: "Are you sure?",   
        text: "This lunch time will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/precio/"+id, true);
        xmlhttp.send();
        showList();
        swal("Deleted!", "The meal time "+id+" has been deleted.", "success"); 
        showList();
    });
    showList();
};


function showList(){
    taskBody = document.getElementById("listBody");

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            var out = "";
            var i;
            if(myArr.length!=0){

                out += '<table class="table table-sm table-hover">'
                    +       '<thead class="thead-inverse">'
                    +           '<tr>'
                    +               '<th class="table-bordered text-center dateColumn">Date</th>'
                    +               '<th class="table-bordered text-center priceColumn">Price</th>'
                    +               '<th class="table-bordered text-center userColumn">Changed By</th>'
                    +               '<th class="table-bordered text-center reasonColumn">Reason</th>'
                    +           '</tr>'
                    +       '</thead>'
                    +   '<tbody id="timesTable">';

                for(i = 0; i < myArr.length; i++) {
                    out += '<tr id="tr' + myArr[i].idPrecio + '">'
                    +           '<td class="text-center">' + myArr[i].Fecha + '</td>'
                    +           '<td class="text-center">' + myArr[i].Precio + '</td>'
                    +           '<td class="text-center">' + myArr[i].idEmpleado + ' - ' + myArr[i].Nombre + ' ' + myArr[i].Paterno + '</td>'
                    +           '<td class="text-center">' + myArr[i].Razon + '</td>';
                    
                    +       '</tr>';     
                }


            out += '</tbody>'
                    +'</table>';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No prices found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
    urlPath = "API/precio/";
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

showList();