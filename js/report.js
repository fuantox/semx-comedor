function createReport() {
    from = document.getElementById("from").value;
    to = document.getElementById("to").value;
    
    if(from==""||(!document.getElementById("weekly").checked&&to=="")){
        sweetAlert("Error", "Please fill all the fields.", "error");
    }
    else if(!document.getElementById("weekly").checked&&from>=to){
        sweetAlert("Error", "Please introduce a valid range of dates.", "error");
    }
    else{
        document.getElementById("create").innerHTML = '<i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Please wait...';
        params = "from="+from;
        if(document.getElementById("weekly").checked == false){
            params += "&to="+to;
        }
        console.log(params);
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "API/reporte/reporte.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && http.status == 200) {
                sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
            }
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                var myArr = JSON.parse(xmlhttp.responseText);
                console.log(xmlhttp.responseText); 
                document.getElementById("download").className = "btn btn-success btn-block";
                console.log(myArr[0]);
            }
        } 
        xmlhttp.send(params);
    }
}

function chWeekRep(){
    weekly = document.getElementById("weekly");
    to = document.getElementById("to");
    if(weekly.checked == true){
        to.disabled = true;
        $('#from').datepicker('destroy');
        $('#from').datepicker({
            format: "yyyy-mm-dd",
            language: "en",
            autoclose: true,
            weekStart: 1,
            daysOfWeekDisabled: '0,2,3,4,5,6'
        });
    }
    else{
        to.disabled = false;
        $('#from').datepicker('destroy');
        $('#from').datepicker({
            format: "yyyy-mm-dd",
            language: "en",
            autoclose: true,
            weekStart: 1
        });
        
        $('#to').datepicker({
            format: "yyyy-mm-dd",
            language: "en",
            autoclose: true,
            weekStart: 1
        });
    }
}

function chTo(){
    $("#create").removeClass("disabled");
}

weekly = document.getElementById("weekly");
weekly.addEventListener('change', chWeekRep);
$("#create").addClass("disabled");
weekly = document.getElementById("create");
weekly.addEventListener('click', createReport);


    