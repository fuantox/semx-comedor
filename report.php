<?php
    require 'template.php';
    session_start();
    if(!isset($_SESSION["id"])){
        header("Location: login.php");
    }
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-datepicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
            navbar();
        ?>
        
        <div class="main-content">
            <div class="container">
                <div class="row searchDiv">
                    <div class="col-md-12">
                        <h1>Consumption Report</h1>
                        <h4>Please select the date range for generating the dinning room consumption report.</h4>
                        <br><br>
                        <div class="row">
                            
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="from" name="from" placeholder="Start Date" onkeypress="return false;"/>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="weekly" checked>&nbsp;Weekly Report
                                    </label>
                                </div>  
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control disabled" id="to" name="to" placeholder="Finish Date" disabled/>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-info btn-block" id="create">
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    &nbsp;Generate Report
                                </a>
                            </div>     
                            <div class="col-md-2">
                                <a class="btn grey btn-success btn-block disabled" id="download" href="API/reporte/rep_<?=$_SESSION["id"];?>.xlsx">
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    &nbsp;Download Report
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <br><br>
                <div class="container listChartContainer" id="listBody">

                </div>
            </div>
        </div>
        
        
        
        <?php
            footer();
        ?>

    </body> 
        
<?php
    scripts();
?>
    <script type="text/javascript" src="js/report.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
    <script>
        $('#from').datepicker({
            format: "yyyy-mm-dd",
            language: "en",
            autoclose: true,
            weekStart: 1,
            daysOfWeekDisabled: '0,2,3,4,5,6'
        });
        $('#from').datepicker()
            .on('changeDate', function(e) {
                chTo();
        });
        
    </script>
    

</html>