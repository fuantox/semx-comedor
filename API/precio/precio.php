<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["id"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        case "DELETE":
            $uri = $_SERVER[REQUEST_URI];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                $id = $params[4];
                $query = "DELETE FROM shokudo.precio WHERE idHorario = '$id'";
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            break;

        case "PUT":
            echo json_encode(["result" => "put"]);
            break;

        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $precio = $_POST["Precio"];
            $razon = $_POST["Razon"];
            $id = $_SESSION["id"];
            $query = "INSERT INTO `shokudo`.`precio`(`Precio`, `Fecha`, `idUsuario`, `Razon`)
                VALUES ('$precio', now(), '$id', '$razon');";        
            if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query, $request->{'id'}]);
                disconnect($conn);
            }
            echo json_encode(["result" => "success"]);
            break;

        case 'GET':
            $query = "SELECT p.idPrecio, p.Precio, DATE(p.Fecha) as Fecha, p.Fecha as Fecha1,  e.idEmpleado, e.Nombre, e.Paterno, p.Razon
                FROM shokudo.precio p, shokudo.empleado e
                WHERE p.idUsuario = e.idEmpleado
                order by Fecha1 desc;";
            $result = $conn->query($query);
            $response = [];

            while ($row = mysqli_fetch_assoc($result)) {
                $response[] = $row;
            }
            echo json_encode($response);
            break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 