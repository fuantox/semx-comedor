<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["id"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
                $query = "DELETE FROM shokudo.empleado WHERE idEmpleado = '$id'";
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
        break;
            
        case "PATCH":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[5]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[5])){
                    $oldId = sprintf('%05d', $params[5]);
                }
                else{
                    $oldId = $params[5];
                }
                if(is_numeric($params[6])){
                    $id = sprintf('%05d', $params[6]);
                }
                else{
                    $id = $params[6];
                }
                $query = "update shokudo.consumo set idEmpleado = '$id', Empresa = 'Sumitomo' where idEmpleado = '$oldId';";
                if ($conn->query($query)) {
                    $query = "update shokudo.shimon set idEmpleado = '$id' where idEmpleado = '$oldId';";
                    if ($conn->query($query)) {
                        if ($conn->query($query)) {
                            $query = "update shokudo.empleado set idEmpleado= '$id', Empresa='Sumitomo' where idEmpleado = '$oldId';";
                            if ($conn->query($query)) {
                                echo json_encode(["result" => "success"]);
                            } else {
                                echo json_encode(["result" => "error0", "msg" => $conn->error]);
                            }
                        } else {
                            echo json_encode(["result" => "error1", "msg" => $conn->error]);
                        }
                    } else {
                        echo json_encode(["result" => "error2", "msg" => $conn->error]);
                    }
                } else {
                    echo json_encode(["result" => "error3", "msg" => $conn->error]);
                } 
            } 
        break;

        case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[5]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[5])){
                    $id = sprintf('%05d', $params[5]);
                }
                else{
                    $id = $params[5];
                }
                $query = "UPDATE `shokudo`.`empleado`
                            SET
                            `Nombre` = '$params[6]',
                            `Paterno` = '$params[7]',
                            `Materno` = '$params[8]',
                            `Empresa` = '$params[9]',
                            `Area` = '$params[10]',
                            `Nomina` = '$params[11]',
                            `Ingreso` = '$params[12]'
                            WHERE `idEmpleado` = '$id';";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $idEmpleado = $_POST['idEmpleado'];
            $Nombre = mysql_escape_mimic($_POST['Nombre']);
            $Paterno = mysql_escape_mimic($_POST['Paterno']);
            if(isset($_POST['Materno'])){
                $Materno = mysql_escape_mimic($_POST['Materno']);
            }
            $Empresa = mysql_escape_mimic($_POST['Empresa']);
            $Area = mysql_escape_mimic($_POST['Area']);
            $Nomina = mysql_escape_mimic($_POST['Nomina']);
            $Ingreso = mysql_escape_mimic($_POST['Ingreso']);
            $query = "INSERT INTO `shokudo`.`empleado`
                (`idEmpleado`, `Nombre`, `Paterno`, `Materno`, `Empresa`, `Area`, `Nomina`, `Ingreso`)
                VALUES ('$idEmpleado', '$Nombre', '$Paterno', '$Materno', '$Empresa', '$Area', '$Nomina', '$Ingreso');";        
            if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query, "id" =>$request['id']]);
                disconnect($conn);
            }
            else{
                $sourcePath = $_SESSION["photo"]; // Storing source path of the file in a variable
                $extension = explode(".", $sourcePath)[1];
                $targetPath = "photo/".$idEmpleado; // Target path where file is to be stored
                rename($sourcePath, $targetPath.".".$extension);
                echo json_encode(["result" => "success"]);
            }
        break;

        case 'GET':
            if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
                $query = "select idEmpleado, Nombre, Paterno, Materno, Empresa, Area, Nomina, Ingreso, Permiso
                        FROM shokudo.empleado
                        WHERE
                        idEmpleado rlike '$term'
                        or Nombre rlike '$term'
                        or Paterno rlike '$term'
                        or Materno rlike '$term';";
                $result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select idEmpleado, Nombre, Paterno, Materno, Empresa
                        FROM shokudo.empleado;";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 