<?php
    session_start();
    //echo json_encode(["call" => "yep"]);
    //echo $_SESSION["id"];
    /*if(!isset($_SESSION["id"])){
        echo json_encode(["result" => "error", "msg" => "Not logged in"]);
    }
    else*/ if(!isset($_POST["from"])&&!isset($_POST["to"])){
        echo json_encode(["result" => "error"]);
    }
    else if(isset($_POST["from"])&&!isset($_POST["to"])){
        $inicio = $_POST["from"];
        $fin = date('Y-m-d', strtotime($_POST["from"]."+6 days"));
        
        //echo "Inicio: ".$inicio;
        //echo "Fin: ".$fin;
        
        $id = $_SESSION["id"];
        
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
        require '../connection.php';

        // Create new PHPExcel object
        //echo date('H:i:s') , " Create new PHPExcel object" , EOL;
        $objPHPExcel = new PHPExcel();

        // Set document properties
        //echo date('H:i:s') , " Set document properties" , EOL;
        $objPHPExcel->getProperties()->setCreator("SEMXSys")
                                     ->setLastModifiedBy("SEMXSys")
                                     ->setTitle("Reporte Comedor")
                                     ->setSubject("Reporte Comedor")
                                     ->setDescription("Reporte Comedor")
                                     ->setKeywords("reporte comedor")
                                     ->setCategory("Reporte Comedor");

        function printReport($pageNum, $title, $tipo, $empresa, $nomina, $objPHPExcel, $inicio, $fin){
            $objPHPExcel->createSheet($pageNum);
            
            $objPHPExcel->setActiveSheetIndex($pageNum);
            $objPHPExcel->getActiveSheet()->setTitle($title);
            
            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('A1', 'Reporte Comedor')
                        ->setCellValue('A2', 'Semana: ')
                        ->setCellValue('B2', $inicio." - ".$fin);
            $objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
            
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('A4', '#NÓMINA')
                        ->setCellValue('B4', 'NOMBRE')
                        ->setCellValue('C4', 'AP. PATERNO')
                        ->setCellValue('D4', 'AP. MATERNO')
                        ->setCellValue('E4', 'EMPRESA')
                        ->setCellValue('F4', 'NÓMINA');

            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('G4:H4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('I4:J4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('K4:L4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('M4:N4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('O4:P4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('Q4:R4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('S4:T4');

            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('U4:V4');

            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('G4', 'LUNES')
                        ->setCellValue('I4', 'MARTES')
                        ->setCellValue('K4', 'MIÉRCOLES')
                        ->setCellValue('M4', 'JUEVES')
                        ->setCellValue('O4', 'VIERNES')
                        ->setCellValue('Q4', 'SÁBADO')
                        ->setCellValue('S4', 'DOMINGO')
                        ->setCellValue('U4', 'TOTAL');

            $objPHPExcel->getActiveSheet()->getStyle("A4:U4")->getFont()->setBold(true);

                $connection=connect();
                $query = "SELECT e.idEmpleado, e.Nombre, e.Paterno, e.Materno, e.Empresa, e.Nomina, c.Precio, DATE(c.Fecha) as Fecha
                FROM shokudo.consumo c, shokudo.empleado e
                where e.idEmpleado = c.idEmpleado
                AND c.Fecha between '$inicio' and '$fin'
                AND e.Empresa like '$empresa'
                AND e.Nomina like '$nomina'
                and c.tipo like b'$tipo'
                order by e.idEmpleado;";
            //echo $query;
                $result=$connection->query($query);

                $excelRow = 4;
                $id = "";
                while($dbRow = $result->fetch_assoc()){
                    if($id != $dbRow["idEmpleado"]){
                        $excelRow++;
                        $excelColumn = 65;
                        $id = $dbRow["idEmpleado"];
                        $nombre = $dbRow["Nombre"];
                        $paterno = $dbRow["Paterno"];
                        $materno = $dbRow["Materno"];
                        $empresa = $dbRow["Empresa"];
                        $nomina = $dbRow["Nomina"];
                        $objPHPExcel->setActiveSheetIndex($pageNum)
                            ->setCellValue(chr($excelColumn++).$excelRow, $id)
                            ->setCellValue(chr($excelColumn++).$excelRow, $nombre)
                            ->setCellValue(chr($excelColumn++).$excelRow, $paterno)
                            ->setCellValue(chr($excelColumn++).$excelRow, $materno)
                            ->setCellValue(chr($excelColumn++).$excelRow, $empresa)
                            ->setCellValue(chr($excelColumn).$excelRow, $nomina);
                    }
                    $quantCol = $excelColumn + date('N', strtotime($dbRow['Fecha'])) + date('N', strtotime($dbRow['Fecha'])) - 1;
                    $cell = chr($quantCol).$excelRow;
                    $cell1 = chr($quantCol+1).$excelRow;
                    $objPHPExcel->setActiveSheetIndex($pageNum)
                            ->setCellValue($cell, "1")
                            ->setCellValue($cell1, $dbRow["Precio"]);

                    $estilo = array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '0B318F')
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'FFFFFF')
                        ),
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array(
                                    'argb' => 'FFFFFF'
                                ),   
                            ),
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->getStyle($cell)->applyFromArray($estilo);
                    $objPHPExcel->getActiveSheet()->getStyle($cell1)->applyFromArray($estilo);

                    $objPHPExcel->setActiveSheetIndex($pageNum)
                            ->setCellValue('U'.$excelRow, "=SUM(G$excelRow, I$excelRow, K$excelRow, M$excelRow, O$excelRow, Q$excelRow, S$excelRow)")
                            ->setCellValue('V'.$excelRow, "=SUM(H$excelRow, J$excelRow, L$excelRow, N$excelRow, P$excelRow, R$excelRow, T$excelRow)");


                } 
        }
        

        printReport(0, "Comidas Quincenal", 0, "Sumitomo", "Quincenal", $objPHPExcel, $inicio, $fin);
        printReport(1, "Comidas Semanal", 0, "Sumitomo", "Semanal", $objPHPExcel, $inicio, $fin);
        printReport(2, "Comidas Capital Humano", 0, "Capital Humano", "Semanal", $objPHPExcel, $inicio, $fin);
        printReport(3, "Breaks Quincenal", 1, "Sumitomo", "Quincenal", $objPHPExcel, $inicio, $fin);
        printReport(4, "Breaks Semanal", 1, "Sumitomo", "Semanal", $objPHPExcel, $inicio, $fin);
        printReport(5, "Breaks Capital Humano", 1, "Capital Humano", "Semanal", $objPHPExcel, $inicio, $fin);
        
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        //echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $filename = 'rep_'.$id.".xlsx";
        $objWriter->save($filename);
        $callEndTime = microtime(true);
        $callTime = $callEndTime - $callStartTime;

        //echo date('H:i:s') , " File written to " , $filename , EOL;
        //echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
        // Echo memory usage
        //echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


        // Echo memory peak usage
        //echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

        // Echo done
        //echo date('H:i:s') , " Done writing files" , EOL;
        //echo 'Files have been created: <a href="'.$filename.'">here.</a>', EOL;
        

        echo json_encode(["result" => "success", "url" => $filename]);
        
    }
    else{
        $inicio = $_POST["from"];
        $fin = $_POST["to"];
        
        //echo "Inicio: ".$inicio;
        //echo "Fin: ".$fin;
        $id = $_SESSION["id"];
        
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
        require '../connection.php';

        // Create new PHPExcel object
        //echo date('H:i:s') , " Create new PHPExcel object" , EOL;
        $objPHPExcel = new PHPExcel();

        // Set document properties
        //echo date('H:i:s') , " Set document properties" , EOL;
        $objPHPExcel->getProperties()->setCreator("SEMXSys")
                                     ->setLastModifiedBy("SEMXSys")
                                     ->setTitle("Reporte Comedor")
                                     ->setSubject("Reporte Comedor")
                                     ->setDescription("Reporte Comedor")
                                     ->setKeywords("reporte comedor")
                                     ->setCategory("Reporte Comedor");

        function printReportW($pageNum, $title, $tipo, $empresa, $nomina, $objPHPExcel, $inicio, $fin){
            $objPHPExcel->createSheet($pageNum);
            
            $objPHPExcel->setActiveSheetIndex($pageNum);
            $objPHPExcel->getActiveSheet()->setTitle($title);
            
            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('A1', 'Reporte Comedor')
                        ->setCellValue('A2', 'Semana: ')
                        ->setCellValue('B2', $inicio." - ".$fin);
            $objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
            
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('A4', '#NÓMINA')
                        ->setCellValue('B4', 'NOMBRE')
                        ->setCellValue('C4', 'AP. PATERNO')
                        ->setCellValue('D4', 'AP. MATERNO')
                        ->setCellValue('E4', 'EMPRESA')
                        ->setCellValue('F4', 'NÓMINA');

            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(7);
            $objPHPExcel->getActiveSheet()->mergeCells('G4:H4');

            $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue('G4', 'TOTAL');

            $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);

                $connection=connect();
                $query = "SELECT e.idEmpleado, e.Nombre, e.Paterno, e.Materno, e.Empresa, e.Nomina, count(*) as Consumos, (CAST(sum(c.Precio) as DECIMAL(10,2))) as Precio, DATE(c.Fecha) as Fecha
                    FROM shokudo.consumo c, shokudo.empleado e
                    where e.idEmpleado = c.idEmpleado
                    AND c.Fecha between '$inicio' and '$fin'
                    AND e.Empresa like '$empresa'
                    AND e.Nomina like '$nomina'
                    and c.tipo like b'$tipo'
                    group by e.idEmpleado
                    order by e.idEmpleado;";
            //echo $query;
                $result=$connection->query($query);

                $excelRow = 4;
                $id = "";
                while($dbRow = $result->fetch_assoc()){
                    $excelRow++;
                    $excelColumn = 65;
                    $id = $dbRow["idEmpleado"];
                    $nombre = $dbRow["Nombre"];
                    $paterno = $dbRow["Paterno"];
                    $materno = $dbRow["Materno"];
                    $empresa = $dbRow["Empresa"];
                    $nomina = $dbRow["Nomina"];
                    $consumos = $dbRow["Consumos"];
                    $precio = $dbRow["Precio"];
                    $objPHPExcel->setActiveSheetIndex($pageNum)
                        ->setCellValue(chr($excelColumn++).$excelRow, $id)
                        ->setCellValue(chr($excelColumn++).$excelRow, $nombre)
                        ->setCellValue(chr($excelColumn++).$excelRow, $paterno)
                        ->setCellValue(chr($excelColumn++).$excelRow, $materno)
                        ->setCellValue(chr($excelColumn++).$excelRow, $empresa)
                        ->setCellValue(chr($excelColumn++).$excelRow, $nomina)
                        ->setCellValue(chr($excelColumn++).$excelRow, $consumos)
                        ->setCellValue(chr($excelColumn).$excelRow, $precio);
                }
                    
        }        

        printReportW(0, "Comidas Quincenal", 0, "Sumitomo", "Quincenal", $objPHPExcel, $inicio, $fin);
        printReportW(1, "Comidas Semanal", 0, "Sumitomo", "Semanal", $objPHPExcel, $inicio, $fin);
        printReportW(2, "Comidas Capital Humano", 0, "Capital Humano", "Semanal", $objPHPExcel, $inicio, $fin);
        printReportW(3, "Breaks Quincenal", 1, "Sumitomo", "Quincenal", $objPHPExcel, $inicio, $fin);
        printReportW(4, "Breaks Semanal", 1, "Sumitomo", "Semanal", $objPHPExcel, $inicio, $fin);
        printReportW(5, "Breaks Capital Humano", 1, "Capital Humano", "Semanal", $objPHPExcel, $inicio, $fin);
        
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        //echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $filename = 'rep_'.$id.".xlsx";
        $objWriter->save($filename);
        $callEndTime = microtime(true);
        $callTime = $callEndTime - $callStartTime;

        //echo date('H:i:s') , " File written to " , $filename , EOL;
        //echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
        // Echo memory usage
        //echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


        // Echo memory peak usage
        //echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

        // Echo done
        //echo date('H:i:s') , " Done writing files" , EOL;
        //echo 'Files have been created: <a href="'.$filename.'">here.</a>', EOL;
        
        echo json_encode(["result" => "success", "url" => $filename]);
    }