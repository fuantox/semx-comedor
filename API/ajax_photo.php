<?php
    session_start();
    if(isset($_FILES["file"]["type"]))
    {
        $validextensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES["file"]["name"]);
        $file_extension = end($temporary);
        if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
            ) && ($_FILES["file"]["size"] < 1000000000)//Approx. 100kb files can be uploaded.
            && in_array($file_extension, $validextensions)) {
            if ($_FILES["file"]["error"] > 0)
            {
                echo 0;
            }   
            else
            {
                if (file_exists("upload/" . $_FILES["file"]["name"])) {
                    echo 1;
                }
                else
                {
                    $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                    $targetPath = "C:/xampp/htdocs/Comedor/upload/".$_FILES['file']['name']; // Target path where file is to be stored
                    move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
                    $_SESSION["photo"] = $targetPath;
                    echo 2;
                    /*echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
                    echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
                    echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
                    echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
                    echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";*/
                }
            }
        }
        else
        {
            echo 3;
        }
    }
else{
    echo 'no';
}
?>