<?php
    require 'template.php';
    session_start();
    if(!isset($_SESSION["id"])){
        header("Location: login.php");
    }
    $name = $_SESSION["name"];
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
    <link type="text/css" rel="stylesheet" href="css/bootstrap-datepicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
        ?>
        
        <div class="main-content">
            <div class="container">
                <div class="row searchDiv">
                    <div class="col-md-12">
                        <h1>Welcome, <?=ucwords(mb_strtolower($name));?>. </h1>
                    </div>               
                </div>
            </div>
        </div>
        
        
        
        <?php
            footer();
        ?>

    </body> 
        
<?php
    scripts();
?>

</html>