<?php
    require 'template.php';
    session_start();
?>

<!DOCTYPE html>
<html class="home">
    <?php 
        head();
    ?>

    <body>
        <?php
            navbar();
        ?>
        
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>

</html>