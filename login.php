<?php
    require 'template.php';
    session_start();
    if(isset($_SESSION["id"])){
        header("Location: main.php");
    }
?>

<!DOCTYPE html>
<html class="login">
    <?php 
        head();
    ?>

    <body>
        <div class="login-form container">
            <div class="row">
                <div class="col-md-6 right-border">
                    <h1 class="logo">
                        <a href="home.php"><img class="logo-sym" src="pics/sumisym.svg">SUMITOMO ELECTRIC SINTERED<br>COMPONENTS MÉXICO</a>
                    </h1>
                </div>
                <form>
                    <div class="col-md-6">
                        <input type="text" class="form-control login-input" placeholder="User ID" id="user" name="user"/>
                        <input type="password" class="form-control login-input" placeholder="Password" id="pass" name="pass"/>
                        <a class="btn btn-primary login-button" onclick="login()">Sign In</a>
                    </div>  
                </form>
            </div>
        </div>
    </body> 

<?php
    scripts();
?>
    <script type="text/javascript" src="js/login.js"></script>
</html>